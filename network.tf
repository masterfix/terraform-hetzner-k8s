resource "hcloud_network" "private" {
  name = "private"
  ip_range = "10.0.0.0/16"
}

resource "hcloud_network_subnet" "private-subnet" {
  type         = "cloud"
  network_id   = hcloud_network.private.id
  network_zone = "eu-central"
  ip_range     = "10.0.1.0/24"
}