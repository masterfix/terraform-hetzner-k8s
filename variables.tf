
variable "hcloud_token" {
  type        = string
  description = "Your Hetzner cloud api token (create it in the web ui)"
}

variable "ssh_keys" {
  type        = map(string)
  default = {
    "patrik@carbonite" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDG4ffcAoK8g/CSOQDaKoZag/DiRFee1Acm4UHZjnzDEVKFrePbwVh+DeMQp3VpVn4ArpNnVdmdq42uHBG5HN+I3Tk4ywKTCz66rx5Po2kCOoQtfp9WgIhxCtOyQwsVmQtzofjYn2+4Xqo9Wn+AkYUy0FQC4YkKdShvqQ/cFRKBkgw9C4w48C281vnmiPKi2ZNR2N4KaYJspF0gIfaiHoTJymdd0EBJQrg3yi/Xh2cd5DRi0xA/w3JN6rBe8u1FxM6pQzIQhhR1WMeGbN0+MA98Is2w2ESfqeCqCll45RKXlT1pa+AyKfCCSG37CxuWirXzHSQValMcu1eIiREReGPdfmSQVGyfzVsbaDgZF9EqI0DhMm8ZNyOHWPqKEj8ys61FUfYe04AXc3sSFjihUHSTrq29dfC78bqVfh0LJZ7KnGa5S57lO9Hazp9rmqwVN8FE9/AVnmSTpbHVKpGc5rRPhOz/0qzfgwrE3p5dfUBPqMevsYjEprUkSB+WxMY2MNU= patrik@carbonite"
    "patrik@rusty.lan" = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQChjVjlq3Mafy6QFnn/1Zz6IJAM/NWklDB998FEblpTot6V5LR1AVnd9YtYMKr3zv0QAe23kEJgvBb6OMSatgkqMscrIor75LQeJ/Ib8AwLrLb5aRnxHw5djKSv4tAHW7/pMegxP0kvhy2jBeK9NuDGmaDBUXiphvR2jxNhbqItUcuDCxeB4gLKE3cK5Oy762ldxYJPeYX9TJmt/OXh3vU+UyXABVV/LeO5Ze3gdVF1+t1bGZ41yzDRjM5fQsnrBz9V5AJ9I2qeUrL/E0MDMOZ4ld36Afr3ks5CiqNwTzbaGvRiLPMQK9jCrrGYsFA1Y+pM2LBz9AbXg1fi4FcOx6308WPBM5nuzForc0qhJwf4iH2UsuOJ2AGacFLzkfzGmrFcCBe15dL2dM65sdBsFXKqUceoqvwvM9/HAnMK+w8Gx7J7mXLI80PAZ2yymM0t7QDYWfw3EuwR5/O2WHJGfDYZEcZTz5r4Hk6gi2MaoNHVb3pxma0FLBvpOZvhorkTZ8c= patrik@rusty.lan"
  }
  description = "SSH public keys for user 'core' and to register on Hetzner Cloud"
}

variable "master_count" {
  type = number
  default = 3
  description = "How many masters to provision?"
}

variable "node_count" {
  type = number
  default = 3
  description = "How many nodes to provision?"
}
